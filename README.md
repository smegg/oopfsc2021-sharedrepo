# oopfsc2021-sharedrepo

As the name suggests, this project serves as a repository for sharing code, notes, or whatever may be interesting.
There is no guarantee for correctness or completeness in the provided files and code.

Please let me know if you would like to share something.
And thanks to all who provide their code for sharing.