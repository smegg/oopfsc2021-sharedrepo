# Exercise sheet 10

The code given in these files is a draft for the solution of exercise sheet 10. It is neither a complete solution, nor fully checked for bugs.

What you can find in the code:
- Three different versions implementing SFINAE for class `Tuple`. They only differ in the implementation of struct `contains`.
  You can choose one in the header of main_tuple.cc.
  Compile with C++14.