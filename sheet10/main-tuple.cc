#include <iostream>
#include <string>

#include "tuple_enum.hh"
// #include "tuple_constexpr.hh"
// #include "tuple_stdtruetype.hh"

int main(int argc, char** argv)
{
  Tuple<int, int, std::string, double> t(1, 2, "Hallo", 3.14);

  // Access by index
  std::cout << "Access by index:\n";
  std::cout << t.entry<0>() << std::endl;
  std::cout << t.entry<1>() << std::endl;
  std::cout << t.entry<2>() << std::endl;
  std::cout << t.entry<3>() << std::endl;

  // Access by type
  std::cout << "\nAccess by type:\n";
  std::cout << t.entry<std::string>() << std::endl;
  std::cout << t.entry<double>() << std::endl;
  // std::cout << t.entry<int>() << std::endl; // COMPILATION ERROR, since 'int' appears more than once
}
