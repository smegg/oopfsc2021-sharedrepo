#ifndef TUPLE_STDTRUETYPE_HH
#define TUPLE_STDTRUETYPE_HH

#include <type_traits>

template<typename... Ts>
struct Tuple;

template<typename T, typename... Ts>
struct Tuple<T, Ts...> : public Tuple<Ts...>
{
  Tuple(const T& value, const Ts&... args)
    : Tuple<Ts...>(args...), value(value)
  {}

  template<std::size_t M, std::enable_if_t<M == 0, bool> = true>
  T& entry()
  {
    return value;
  }

  template<std::size_t M, std::enable_if_t<M != 0, bool> = true>
  auto& entry()
  {
    return Tuple<Ts...>::template entry<M-1>();
  }

  template<typename U, std::enable_if_t<std::is_same<T, U>::value
					&
					!Tuple<Ts...>::template contains<U>::value, bool> = true>
  U& entry()
  {
    return value;   
  }

  template<typename U, std::enable_if_t<!std::is_same<T, U>::value, bool> = true>
  auto& entry()
  {
    return Tuple<Ts...>::template entry<U>();
  }

  // Structs needed for SFINAE type deduction
  template<bool b> struct booltype {}; // Helper struct used in struct contains

  template<typename U, typename = booltype<true> >
  struct contains : std::false_type {};

  template<typename U>
  struct contains<U, booltype<std::is_same<U, T>::value
  			      || Tuple<Ts...>::template contains<U>::value>
  		  > : std::true_type {};

private:
  T value;
};

template<>
struct Tuple<>
{
  template<typename>
  struct contains : std::false_type { };
};

#endif // !TUPLE_STDTRUETYPE_HH
