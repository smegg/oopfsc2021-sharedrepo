# Exercise sheet 5

The code given in these files is a draft for the solution of exercise sheet 5. It is neither a complete solution, nor fully checked for bugs.

What you can find in the code:
- template template parameter (optionally with parameter packs)
- usage of SFINAE by `std::enable_if` in class `SimpleMatrix`
- possibilities for debugging:
  - exception handling with `try`/`catch` block or (commented right now, but in the code)
  - debug mode for run-time checks by using preprocessor instructions `#ifdef`, `#define` and `#endif`.
    For using debugging mode compile with -DDEBUG flag as:
        g++ -std=c++11 -DDEBUG -o testmatrix testmatrix.cc
    If you don't want to use it simply compile with
        g++ -std=c++11 -o testmatrix testmatrix.cc
    Otherwise you can uncomment `#define DEBUG` in file matrix.templates.h (line 4)
