#ifndef matrix_h
#define matrix_h

#include<array>
#include<vector>
#include<iostream>
#include<type_traits>

template<typename T, int numRows, int numCols = numRows>
class SimpleMatrix
{
public:
  SimpleMatrix();
  SimpleMatrix(T value);
  SimpleMatrix(const std::vector<std::vector<T> >& a);
  SimpleMatrix(const SimpleMatrix& b);
  // ACCESS ELEMENTS
  T& operator()(unsigned int i, unsigned int j);
  T  operator()(unsigned int i, unsigned int j) const;
  std::vector<T>& operator[](unsigned int i);
  const std::vector<T>& operator[](unsigned int i) const;
  // OUTPUT
  // print function for arithmetic type or string
  template<typename U = T>
  typename std::enable_if<std::is_arithmetic<U>::value
                          || std::is_same<U,std::string >::value>::type
  print() const;

  // print function for everything else
  template<typename U = T>
  typename std::enable_if<!(std::is_arithmetic<U>::value
  			    || std::is_same<U,std::string >::value)>::type
  print() const;

  unsigned int rows() const;
  unsigned int cols() const;
private:
  std::vector<std::vector<T> > entries;
};

template<typename T, int numRows, int numCols>
class NumMatrix : public SimpleMatrix<T,numRows,numCols>
{
public:
  NumMatrix();
  NumMatrix(T value);
  NumMatrix(const std::vector<std::vector<T> >& a);
  NumMatrix(const NumMatrix& b);
  NumMatrix(const SimpleMatrix<T,numRows,numCols>& b);
  // ARITHMETIC FUNCTIONS
  NumMatrix& operator*=(T x);
  NumMatrix& operator+=(const NumMatrix& b);
};


// FREE FUNCTIONS
template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numRows> operator+(const SimpleMatrix<T,numRows,numRows>& a,
				       const SimpleMatrix<T,numRows,numRows>& b);

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numRows> operator*(const SimpleMatrix<T,numRows,numRows>& a,
				       T x);

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numRows> operator*(T x,
				       const SimpleMatrix<T,numRows,numRows>& a);

/**
 * 1st version of matrix-vector multiplication for dynamic containers
 * like std::vector<T>
 */
template<typename T, int numRows, int numCols, class C>
C operator*(const SimpleMatrix<T,numRows,numRows>& a,
	    const C& x);

/**
 * 2nd verion of matrix-vector multiplication for dynamic containers
 * like std::vector<T>
 */
// template<typename T, int numRows, int numCols, template<typename...> class C>
// C<T> operator*(const SimpleMatrix<T,numRows,numCols>& a,
// 	       const C<T>& x);

/**
 * Matrix-vector multiplication for static containers like
 * std::array<T>
 */
template<typename T, int numRows, int numCols, template<typename, size_t> class C>
C<T,numRows> operator*(const SimpleMatrix<T,numRows,numCols>& a,
		       const C<T,numCols>& x);


#endif	// matrix_h
