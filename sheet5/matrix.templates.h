#ifndef matrix_templates_h
#define matrix_templates_h

// #define DEBUG

#include "matrix.h"
#include<iomanip>
#include<iostream>
#include<cstdlib>

/************************/
/***** SimpleMatrix *****/
/************************/

template<typename T, int numRows, int numCols>
SimpleMatrix<T,numRows,numCols>::SimpleMatrix()
  : entries(std::vector<std::vector<T>>(numRows,std::vector<T>(numCols)))
{}

template<typename T, int numRows, int numCols>
SimpleMatrix<T,numRows,numCols>::SimpleMatrix(T value)
  : entries(std::vector<std::vector<T>>(numRows,std::vector<T>(numCols,value)))
{}

template<typename T, int numRows, int numCols>
SimpleMatrix<T,numRows,numCols>::SimpleMatrix(const std::vector<std::vector<T> >& a)
  : entries(a)
{}

template<typename T, int numRows, int numCols>
SimpleMatrix<T,numRows,numCols>::SimpleMatrix(const SimpleMatrix& b)
  : entries(b.entries)
{}

template<typename T, int numRows, int numCols>
T& SimpleMatrix<T,numRows,numCols>::operator()(unsigned int i, unsigned int j)
{
  return entries[i][j];
}

template<typename T, int numRows, int numCols>
T SimpleMatrix<T,numRows,numCols>::operator()(unsigned int i, unsigned int j) const
{
  return entries[i][j];
}

template<typename T, int numRows, int numCols>
std::vector<T>& SimpleMatrix<T,numRows,numCols>::operator[](unsigned int i)
{
  return entries[i];
}

template<typename T, int numRows, int numCols>
const std::vector<T>& SimpleMatrix<T,numRows,numCols>::operator[](unsigned int i) const
{
  return entries[i];
}

template<typename T, int numRows, int numCols>
template<typename U>
typename std::enable_if<std::is_arithmetic<U>::value || std::is_same<U,std::string >::value>::type
SimpleMatrix<T,numRows,numCols>::print() const
{
  std::cout << "(" << numRows << "x";
  std::cout << numCols << ") matrix:" << std::endl;
  for (size_t i = 0; i < numRows; ++i)
    {
      std::cout << std::setprecision(3);
      for (size_t j = 0; j < numCols; ++j)
	std::cout << std::setw(5) << entries[i][j] << " ";
      std::cout << std::endl;
    }
  std::cout << std::endl;
}

template<typename T, int numRows, int numCols>
template<typename U>
typename std::enable_if<!(std::is_arithmetic<U>::value || std::is_same<U,std::string >::value)>::type
SimpleMatrix<T,numRows,numCols>::print() const
{
  std::cout << "(" << numRows << "x";
  std::cout << numCols << ") matrix:" << std::endl;
  for (size_t i = 0; i < numRows; ++i)
    {
      std::cout << std::setprecision(3);
      for (size_t j = 0; j < numCols; ++j)
	std::cout << std::setw(5) << "B ";
      std::cout << std::endl;
    }
  std::cout << std::endl;
}

template<typename T, int numRows, int numCols>
unsigned int SimpleMatrix<T,numRows,numCols>::rows() const
{
  return numRows;
}

template<typename T, int numRows, int numCols>
unsigned int SimpleMatrix<T,numRows,numCols>::cols() const
{
  return numCols;
}


/*********************/
/***** NumMatrix *****/
/*********************/

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>::NumMatrix()
  : SimpleMatrix<T,numRows,numCols>()
{}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>::NumMatrix(T value)
  : SimpleMatrix<T,numRows,numCols>(value)
{}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>::NumMatrix(const std::vector<std::vector<T> >& a)
  : SimpleMatrix<T,numRows,numCols>(a)
{}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>::NumMatrix(const NumMatrix& b)
  : SimpleMatrix<T,numRows,numCols>(b)
{}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>::NumMatrix(const SimpleMatrix<T,numRows,numCols>& b)
  : SimpleMatrix<T,numRows,numCols>(b)
{}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>& NumMatrix<T,numRows,numCols>::operator*=(T x)
{
  for (size_t i = 0; i < numRows; ++i)
    for (size_t j = 0; j < numCols; ++j)
      (*this)(i,j) *= x;
  return *this;
}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols>& NumMatrix<T,numRows,numCols>::operator+=(const NumMatrix& x)
{
  for (size_t i = 0; i < numRows; ++i)
    for (size_t j = 0;j < numCols; ++j)
      (*this)(i,j) += x(i,j);
  return *this;
}


/*********************/
/*** Free functions **/
/*********************/

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols> operator+(const SimpleMatrix<T,numRows,numCols>& a, const SimpleMatrix<T,numRows,numCols>& b)
{
  NumMatrix<T,numRows,numCols> output(a);
  output += b;
  return output;
}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols> operator*(const SimpleMatrix<T,numRows,numCols>& a, T x)
{
  NumMatrix<T,numRows,numCols> output(a);
  output *= x;
  return output;
}

template<typename T, int numRows, int numCols>
NumMatrix<T,numRows,numCols> operator*(T x, const SimpleMatrix<T,numRows,numCols>& a)
{
  NumMatrix<T,numRows,numCols> output(a);
  output *= x;
  return output;
}

template<typename T, int numRows, int numCols, class C>
C operator*(const SimpleMatrix<T,numRows,numCols>& a,
	    const C& x)
{
#ifdef DEBUG
  std::cout << "*************************************\n"
	    << "******* DEBUGGING INFORAMATION ******\n"
	    << "*************************************\n\n";
  if (x.size() != a.cols())
    {
      std::cerr << "Dimensions of vector " << x.size();
      std::cerr << " and matrix " << a.cols() << " do not match!";
      std::cerr << std::endl;
      exit(EXIT_FAILURE);
    }
#endif // DEBUG

  // try {
    C y(numRows);
    for (size_t i = 0; i < numRows; ++i)
      {
	y[i] = 0;
	for (size_t j = 0; j < numCols; ++j)
	  y[i] += a(i,j) * x.at(j);
	  // y[i] += a(i,j) * x[j];
      }
    return y;

  // } catch (const std::exception& e) { // caught by reference to base
  //   std::cerr << "Dimensions of vector " << x.size();
  //   std::cerr << " and matrix " << a.cols() << " do not match! ";
  //   std::cerr << e.what() << "'\n";
  //   exit(EXIT_FAILURE);
  // }
}

// template<typename T, int numRows, int numCols, template<typename...> class C>
// C<T> operator*(const SimpleMatrix<T,numRows,numCols>& a,
// 	       const C<T>& x)
// {
//   C<T> y(numRows);
//   for (size_t i = 0; i < numRows; ++i)
//     {
//       y[i] = static_cast<T>(0.);
//       for (size_t j = 0; j < numCols; ++j)
// 	y[i] += a[i][j] * x[j];
//     }
//   return y;
// }

template<typename T, int numRows, int numCols, template<typename,size_t> class C>
C<T,numRows> operator*(const SimpleMatrix<T,numRows,numCols>& a,
		       const C<T,numCols>& x)
{
  C<T,numRows> y;
  for (size_t i = 0; i < numRows; ++i)
    {
      y[i] = 0;
      for (size_t j = 0; j < numCols; ++j)
	y[i] += a(i,j) * x[j];
    }
  return y;
}


#endif	// matrix_templates_h
