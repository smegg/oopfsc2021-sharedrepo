#include "matrix.templates.h"
#include<iostream>

template<typename T>
void test()
{
  // define matrix
  const int arows = 4;
  const int acols = 6;

  NumMatrix<T,arows,acols> A(0.);
  for (size_t i = 0; i < A.rows(); ++i)  
    A[i][i] = 2.;
  for (size_t i=0; i < A.rows()-1; ++i) 
    A[i+1][i] = A[i][i+1] = -1.;

  // print matrix
  A.print();

  // define matrix
  SimpleMatrix<T,6,4> B(0.);
  for (size_t i = 0; i <B.cols(); ++i)  
    B[i][i] = 2.;
  for (size_t i = 0; i < B.cols()-1; ++i) 
    B[i+1][i] = B[i][i+1] = -1.;

  // print matrix
  B.print();

  NumMatrix<T,arows,acols> C(A);
  A = static_cast<T>(2) * C;
  A.print();
  A = C * static_cast<T>(2);
  A.print();
  A = C + A;
  A.print();

  std::cout << "Element 1,1 of A is " << A(1,1) << std::endl;
  std::cout << std::endl;

  // matrix-vector multiplication
  std::vector<T> b(acols);
  b[0] = 5.;
  b[1] = b[3] = -4.;
  b[2] = 4.;
  std::vector<T> x = A * b;
  std::cout << "Matrix*std::vector<T>: A*b = ( ";
  for (size_t i = 0; i < x.size(); ++i)
    std::cout << x[i] << "  ";
  std::cout << ")" << std::endl;
  std::cout << std::endl;

  // matrix-array multiplication
  std::array<T,4> c = {1,2,3,4};
  std::array<T,6> y = (B * c);
  std::cout << "Matrix*std::array<T,N>: B*c = ( ";
  for (size_t i = 0; i < y.size(); ++i)
    std::cout << y[i] << "  ";
  std::cout << ")" << std::endl;
  std::cout << std::endl;

  // // failing matrix-vector multiplication
  // std::vector<T> b1(2);
  // b1[0] = 1.;
  // b1[1] = 2.;
  // std::vector<T> x1 = A * b1;
  // std::cout << "Matrix*std::vector<T>: A*b = ( ";
  // for (size_t i = 0; i < x1.size(); ++i)
  //   std::cout << x1[i] << "  ";
  // std::cout << ")" << std::endl;
  // std::cout << std::endl;

}

int main()
{
  std::cout << "//////////////////////\n";
  std::cout << "/////// double ///////\n";
  std::cout << "//////////////////////\n";
  test<double>();

  std::cout << "//////////////////////////\n";
  std::cout << "// SimpleMatrix<string> //\n";
  std::cout << "//////////////////////////\n";
  std::string str = "my_string";
  SimpleMatrix<std::string,6,4> S(str);
  S.print();


  std::cout << "//////////////////////////////////\n";
  std::cout << "// SimpleMatrix<NumMatrix<int>> //\n";
  std::cout << "//////////////////////////////////\n";
  NumMatrix<int,2,2> M(2);

  SimpleMatrix<NumMatrix<int,2,2>,6,4> N(M);
  N.print();


  std::cout << "//////////////////////////////////\n";
  std::cout << "// NumMatrix<NumMatrix<double>> //\n";
  std::cout << "//////////////////////////////////\n";
  NumMatrix<double,4,6> A(0.);
  for (size_t i = 0; i < A.rows(); ++i)  
    A[i][i] = 2.;
  for (size_t i=0; i < A.rows()-1; ++i) 
    A[i+1][i] = A[i][i+1] = -1.;

  NumMatrix<NumMatrix<double,4,6>,6,4> O(A);
  O.print();
}
