# EXAMPLE CODE FOR SFINAE

What you can find in the code:
- Old-fashioned and C++11-style examples from the slides (p. 149-159)
- Old-fashioned example needs an implementation of `enable_if`, if you
want to compile it with an older version than C++11

Remarks:
- Pointer to member function `T1::*`
- Works only for classes not for arithmetic types
- Does not work directly for derived classes
