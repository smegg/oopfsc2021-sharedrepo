/**
 * EXAMPLE CODE FOR SFINAE
 * - C++11-style examples from the slides (p. 149-159)
 *
 * Remarks:
 * - Pointer to member function T1::*
 * - Works only for classes not for arithmetic types
 * - Does not work directly for derived classes
 **/

#include <iostream>

using namespace std;

// type trait implementation from p. 157
template<typename T1, typename T2>
struct hasMultiplyWithInverse
{
  template<typename T, typename U = void> // (1)
  struct Helper {enum {value = false};};

  template<typename T>		// (2)
  struct Helper<T,decltype(&T::multiplyWithInverse)> {enum {value = true};};

  // matches (2) if function exists, else matches (1)
  enum {value = Helper<T1,void (T1::*)(T2&) const>::value};
};

// classes Vector, Matrix, InvertibleMatrix, SpecialInvertibleMatrix
class Vector{};
class Matrix{};

template<typename V>
class InvertibleMatrix
{
public:
  void multiplyWithInverse(V&) const {std::cout << "multiplyWithInverse()\n";};
};

template<typename V>
class SpecialInvertibleMatrix : public InvertibleMatrix<V>
{};

// SFINAE implementation of solve()
template<typename M, typename V>
typename enable_if<hasMultiplyWithInverse<M,V>::value>::type
solve(const M& m, V& v)
{
  m.multiplyWithInverse(v);
}

template<typename M, typename V>
typename enable_if<!hasMultiplyWithInverse<M,V>::value>::type
solve(const M&, V&)
{
  std::cout << "multiplyWithInverse() not available\n";
}

int main()
{
  Vector v;
  Matrix m;
  InvertibleMatrix<Vector> im;
  SpecialInvertibleMatrix<Vector> sm;

  solve(m, v);		     // doesn't have multiplyWithInverse()

  solve(im, v);		     // has multiplyWithInverse()

  // solve(1.0, v);	     // doesn't compile due to pointer to member function T1::*

  sm.multiplyWithInverse(v); // multiplyWithInverse() from Base class is called

  solve(sm, v);		     // Actually a bug: Derived class has
			     // multiplyWithInverse() from Base, but
			     // pointer to member function does not
			     // consider it
}

/**
 * EXPECTED OUTPUT:
 * multiplyWithInverse() not available
 * multiplyWithInverse()
 * multiplyWithInverse()
 * multiplyWithInverse() not available   (bug)
*/
