/**
 * EXAMPLE CODE FOR SFINAE
 * - Hack that uses the comma-operator inside decltype and
 *   std::false_type and std::true_type as base classes (where value
 *   is implemented)
 * - Compiles with C++11
 *
 * Remarks:
 * - Uses address of member &T::method
 * - Works also for arithmetic types
 * - Works also for derived classes
 **/

#include <iostream>

using namespace std;

// type trait implementation using comma operator inside decltype
template<typename T, typename V, typename = int>
struct hasMultiplyWithInverse : std::false_type
{};

template<typename T, typename V>
struct hasMultiplyWithInverse<T, V, decltype(&T::multiplyWithInverse, 0)> : std::true_type
{};

// classes Vector, Matrix, InvertibleMatrix, SpecialInvertibleMatrix
class Vector{};
class Matrix{};

template<typename V>
class InvertibleMatrix
{
public:
  void multiplyWithInverse(V&) const {std::cout << "multiplyWithInverse()\n";};
};

template<typename V>
class SpecialInvertibleMatrix : public InvertibleMatrix<V>
{};

// SFINAE implementation of solve()
template<typename M, typename V>
typename enable_if<hasMultiplyWithInverse<M,V>::value>::type
solve(const M& m, V& v)
{
  m.multiplyWithInverse(v);
}

template<typename M, typename V>
typename enable_if<!hasMultiplyWithInverse<M,V>::value>::type
solve(const M&, V&)
{
  std::cout << "multiplyWithInverse() not available\n";
}

int main()
{
  Vector v;
  Matrix m;
  InvertibleMatrix<Vector> im;
  SpecialInvertibleMatrix<Vector> sm;

  solve(m, v);		     // doesn't have multiplyWithInverse()
  solve(im, v);		     // has multiplyWithInverse()
  solve(1.0, v);	     // compiles now and doesn't have multiplyWithInverse()
  sm.multiplyWithInverse(v); // multiplyWithInverse() from Base class is called
  solve(sm, v);		     // This time, derived class considers multiplyWithInverse()
			     // from Base
}

/**
 * EXPECTED OUTPUT:
 * multiplyWithInverse() not available
 * multiplyWithInverse()
 * multiplyWithInverse() not available
 * multiplyWithInverse()
 * multiplyWithInverse()
*/
