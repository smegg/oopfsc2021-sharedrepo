/**
 * EXAMPLE CODE FOR SFINAE
 * - Old-fashioned (pre C++11-style) examples from the slides (p. 149-159)
 * - Needs an implementation of enable_if, if you want to compile it
 *   with an older version than C++11 (like C++98)
 *
 * Remarks:
 * - Pointer to member function T1::*
 * - Works only for classes not for arithmetic types
 * - Does not work directly for derived classes
 **/

#include <iostream>

using namespace std;

// enable_if implementation from p. 150
template<bool B, class T = void>
struct enable_if {};

template<class T>
struct enable_if<true, T> {typedef T type;};

// type trait implementation from p. 154
template <class T1, class T2>
struct hasMultiplyWithInverse
{
  typedef struct{char a; char b;} yes; // size 2
  typedef struct{char a;}         no;  // size 1

  template <typename U, U u> struct isMethod;

  template <typename C1, typename C2>
  static yes test(isMethod<void (C1::*)(C2&) const, &C1::multiplyWithInverse>*) {}

  template <typename,typename> static no test(...) {}

  enum {value = (sizeof(test<T1,T2>(0)) == sizeof(yes))};
};

// classes Vector, Matrix, InvertibleMatrix, SpecialInvertibleMatrix
class Vector{};
class Matrix{};

template<typename V>
class InvertibleMatrix
{
public:
  void multiplyWithInverse(V&) const {std::cout << "multiplyWithInverse()\n";};
};

template<typename V>
class SpecialInvertibleMatrix : public InvertibleMatrix<V>
{};

// SFINAE implementation of solve()
template<typename M, typename V>
typename enable_if<hasMultiplyWithInverse<M,V>::value>::type
solve(const M& m, V& v)
{
  m.multiplyWithInverse(v);
}

template<typename M, typename V>
typename enable_if<!hasMultiplyWithInverse<M,V>::value>::type
solve(const M&, V&)
{
  std::cout << "multiplyWithInverse() not available\n";
}

int main()
{
  Vector v;
  Matrix m;
  InvertibleMatrix<Vector> im;
  SpecialInvertibleMatrix<Vector> sm;

  solve(m, v);		     // doesn't have multiplyWithInverse()

  solve(im, v);		     // has multiplyWithInverse()

  // solve(1.0, v);	     // doesn't compile due to pointer to member function T1::*

  sm.multiplyWithInverse(v); // multiplyWithInverse() from Base class is called

  solve(sm, v);		     // Actually a bug: Derived class has
			     // multiplyWithInverse() from Base, but
			     // pointer to member function does not
			     // consider it
}

/**
 * EXPECTED OUTPUT:
 * multiplyWithInverse() not available
 * multiplyWithInverse()
 * multiplyWithInverse()
 * multiplyWithInverse() not available  (bug)
*/
