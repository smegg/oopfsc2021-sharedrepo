#include <iostream>
#include <iomanip>
#include <functional>
#include <vector>
#include <string>
#include <cmath>

auto q_trapezoidal = [](std::function<double(double)> f,
			const double a,
			const double b)
{
  return (b-a)/2. * (f(a) + f(b));
};

auto q_simpson = [](std::function<double(double)> f,
		    const double a,
		    const double b)
{
  return (b-a)/6. * (f(a) + 4.*f((a+b)/2.) + f(b));
};

auto integrate = [](std::function<double(double)> f,
		    const double a,
		    const double b,
		    const unsigned int n,
		    std::function<double(std::function<double(double)>,
					 const double,
					 const double)> quadrature)
{
  const double dt = (b-a)/(double(n));
  double integral = 0.;
  for (unsigned int i=0; i<n; ++i)
    integral += quadrature(f, a+double(i)*dt, a+double(i+1)*dt);
  return integral;
};

auto test = [](std::function<double(double)> f,
	       const unsigned int N,
	       std::function<double(std::function<double(double)>,
				    const double,
				    const double)> quadrature,
	       const double exactvalue)
{
  std::cout
  << std::setprecision(0) << std::fixed
  << std::setw(5) << "n"
  << std::setprecision(2) << std::fixed
  << std::setw(10) << "integral"
  << std::scientific
  << std::setw(10) << "error" << "\n";

  for (unsigned int n=0; n<N; ++n)
    {
      const double integral = integrate(f, -1., 3., std::pow(2,n), quadrature);
      const double error = std::abs(integral-exactvalue);

      std::cout << std::setprecision(0) << std::fixed
		<< std::setw(5) << std::pow(2,n)
		<< std::setprecision(2) << std::fixed
		<< std::setw(10) << integral
		<< std::scientific
		<< std::setw(10) << error << "\n";
      // EOC
    }
  std::cout << "\n";
};

int main()
{
  std::vector<std::string>
    testcases = {"x + 0.5",
		 "2.*x^2 + 5",
		 "-sin(x) + 4.*x^2 + 2"};

  std::vector<std::vector<double> >
    params = {{0., 0., 1., 0.5},
	      {0., 2., 0., 5.},
	      {-1., 4., 0., 2.}};

  std::vector<double>
    exactvalues = {6.,
		   116./3.,
		   136./3.-2.*std::sin(1.)*std::sin(2.)};

  for (unsigned int i=0; i<3; ++i)
  {
    const double a = params[i][0];
    const double b = params[i][1];
    const double c = params[i][2];
    const double d = params[i][3];
    auto f = [a,b,c,&d](double x){return a*std::sin(x) + b*x*x + c*x + d;};

    std::cout << "Test case: " << testcases[i] << "\n";
    test(f, 10, q_trapezoidal, exactvalues[i]);
    test(f, 10, q_simpson, exactvalues[i]);
    std::cout << "\n";
  }
}
