#include<iostream>
#include<vector>

// type trait
template <typename T, typename = int>
struct has_print : std::false_type
{};

template <typename T>
struct has_print<T, decltype(&T::print, 0)> : std::true_type
{};

// Template specializations
template<typename T,
	 typename std::enable_if<has_print<T>::value, bool>::type = true>
void print(T t)
{
  t.print();
}

template<typename T,
	 typename std::enable_if<std::is_floating_point<T>::value, bool>::type = true>
void print(T f)
{
  auto precision = std::cout.precision();
  std::cout.precision(6);
  std::cout << std::scientific << f << std::endl;
  std::cout.precision(precision);
}

template<typename T,
	 typename std::enable_if<!std::is_floating_point<T>::value
				 & !has_print<T>::value, bool>::type = true>
void print(T t)
{
  std::cout << t << std::endl;
}

// Overloads
void print(std::string s)
{
  std::cout << "\"" << s << "\"\n";
}

void print(const char* c)
{
  std::cout << "\"" << c << "\"\n";
}

// Variadic
template<typename T, typename... Ts>
void print(T head, Ts... tail)
{
  print(head);
  print(tail...);
}

// class with print method
template<typename T>
class Vector {
public:
  Vector(const std::vector<T>& v) : entries(v){}
  void print()
  {
    std::cout << "[" << entries[0];
    for (unsigned int i=0; i<entries.size(); ++i)
      std::cout << "," << entries[i];
    std::cout << "]\n";
  }
private:
  std::vector<T> entries;
};

int main()
{
  Vector<double> v({1.2,3.,4.719});
  std::string s = "stringHello";
  print("Hello", v, 2, 3.14,s);
}
